package com.emirilda.spigotmc.cornerstones;

import com.emirilda.spigotmc.cornerstones.commands.Command_Chunk;
import com.emirilda.spigotmc.cornerstones.commands.Command_Main;
import com.emirilda.spigotmc.cornerstones.commands.Command_Set;
import com.emirilda.spigotmc.cornerstones.events.CornerstoneUnloadEvent;
import com.emirilda.spigotmc.cornerstones.events.GuiListener;
import com.emirilda.spigotmc.cornerstones.events.InteractEvent;
import com.emirilda.spigotmc.cornerstones.events.SignInteractEvent;
import com.emirilda.spigotmc.cornerstones.events.connection.ConnectionEvents;
import com.emirilda.spigotmc.cornerstones.events.griefprotection.*;
import com.emirilda.spigotmc.cornerstones.managers.GuiManager;
import com.emirilda.spigotmc.cornerstones.managers.SavesManager;
import com.emirilda.spigotmc.cornerstones.utility.Messages;
import com.emirilda.spigotmc.cornerstones.utility.Settings;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.*;
import com.emirilda.spigotmc.emirilda.Emirilda;
import com.emirilda.spigotmc.emirilda.commands.builder.CommandBuilder;
import com.emirilda.spigotmc.emirilda.database.DatabaseManager;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import com.emirilda.spigotmc.emirilda.datablocks.DataBlockManager;
import com.emirilda.spigotmc.emirilda.managers.ConfigManager;
import com.emirilda.spigotmc.emirilda.managers.LanguageManager;
import com.emirilda.spigotmc.emirilda.utility.TextUtility;
import com.emirilda.spigotmc.emirilda.utility.interfaces.Config;
import lombok.Getter;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.structure.StructureManager;

import java.io.File;
import java.util.*;

public final class Main extends JavaPlugin {

    @Getter
    private static String pluginName, pluginVersion, pluginDescription, pluginAuthors;
    @Getter
    private static Main instance;

    @Getter
    private static ConfigManager configManager;
    @Getter
    private static LanguageManager languageManager;

    public static Config config;

    @Getter
    private static DatabaseManager databaseManager;

    @Getter
    private static DataBlockManager dataBlockManager;

    @Getter
    private static HashMap<UUID, QueueUser> userQueue;
    @Getter //ChunkId - Cornerstone
    private static HashMap<String, Cornerstone> cornerstones;
    @Getter //Player uuid - ChunkId
    private static HashMap<UUID, User> users;

    @Getter
    private static StructureManager structureManager;

    @Getter
    private static GuiManager guiManager;
    @Getter
    private static HashMap<String, YamlInventory> inventories;
    @Getter
    private static HashMap<UUID, Gui> playerGuis;
    @Getter
    private static List<UUID> playerCooldowns;

    @Override
    public void onEnable() {
        /* Initializing information and classes */
        pluginName = getDescription().getName();
        pluginVersion = getDescription().getVersion();
        pluginDescription = getDescription().getDescription();
        pluginAuthors = TextUtility.readableJoin(getDescription().getAuthors().toArray(new String[0]));
        instance = this;

        structureManager = getServer().getStructureManager();


        clog("""
                                
                                
                                
                                
                 █████╗  █████╗ ██████╗ ███╗  ██╗███████╗██████╗  ██████╗████████╗ █████╗ ███╗  ██╗███████╗ ██████╗
                ██╔══██╗██╔══██╗██╔══██╗████╗ ██║██╔════╝██╔══██╗██╔════╝╚══██╔══╝██╔══██╗████╗ ██║██╔════╝██╔════╝
                ██║  ╚═╝██║  ██║██████╔╝██╔██╗██║█████╗  ██████╔╝╚█████╗    ██║   ██║  ██║██╔██╗██║█████╗  ╚█████╗
                ██║  ██╗██║  ██║██╔══██╗██║╚████║██╔══╝  ██╔══██╗ ╚═══██╗   ██║   ██║  ██║██║╚████║██╔══╝   ╚═══██╗
                ╚█████╔╝╚█████╔╝██║  ██║██║ ╚███║███████╗██║  ██║██████╔╝   ██║   ╚█████╔╝██║ ╚███║███████╗██████╔╝
                 ╚════╝  ╚════╝ ╚═╝  ╚═╝╚═╝  ╚══╝╚══════╝╚═╝  ╚═╝╚═════╝    ╚═╝    ╚════╝ ╚═╝  ╚══╝╚══════╝╚═════╝
                                                  █▀▀▄ █  █ 　 █▀▀█ █    ▀  █▀▀█ █▀▀ █▀▀
                                            ▀▀▀▀  █▀▀▄ █▄▄█ 　   ▀▄ █   ▀█▀ █▄▄█ ▀▀█ ▀▀█  ▀▀▀▀
                                                  ▀▀▀  ▄▄▄█ 　 █▄▄█ ▀▀▀ ▀▀▀ ▀  ▀ ▀▀▀ ▀▀▀
                                                  
                    """);

        //Loading config
        clog("Loading config.yml...");
        configManager = new ConfigManager(this);
        configManager.getConfig("config.yml").copyFromResource();
        config = configManager.getConfig("config.yml");

        //Loading language file
        clog("Loading language file...");
        languageManager = new LanguageManager(this, config);

        //Setting up databaseManager
        File dataFolder = new File(getDataFolder(), "data");
        if (!dataFolder.exists()) {
            if (dataFolder.mkdirs()) clog("Creating data folder...");
        }
        if (Settings.MYSQL.getBoolean()) {
            clog("Connecting to MySQL...");
            databaseManager = new DatabaseManager(getInstance(), Settings.DATABASE_DATABASE.getString(),
                    Settings.DATABASE_HOST.getString(), Settings.DATABASE_USERNAME.getString(),
                    Settings.DATABASE_PASSWORD.getString(), Settings.DATABASE_PORT.getInt());
            clog("Successfully connected to MySQL.");
        } else {
            clog("Connecting to SQLite...");
            databaseManager = new DatabaseManager(getInstance(), "data/saves.db");
            clog("Successfully connected to SQLite.");
        }

        //Setting up dataBlockManager
        clog("Connecting to DataBlock SQLite...");
        DatabaseManager dataBlockDatabaseManager = new DatabaseManager(getInstance(), "data/datablocks.db");
        dataBlockManager = new DataBlockManager(dataBlockDatabaseManager);
        clog("Successfully connected to DataBlock SQLite.");

        //Initializing SavesManager
        SavesManager.init();

        //Initializing maps and lists
        userQueue = new HashMap<>();
        cornerstones = new HashMap<>();
        users = new HashMap<>();
        playerCooldowns = new ArrayList<>();


        //Loading cornerstones
        clog("Loading cornerstones...");
        SavesManager.loadAllChunks();

        clog("Loading inventories...");
        guiManager = new GuiManager();
        inventories = guiManager.loadInventories();
        playerGuis = new HashMap<>();


        /* Registering commands */

        clog("Loading and building commands...");

        CommandBuilder commandBuilder = new CommandBuilder(this);
        commandBuilder.load("commands/commands.yml");
        commandBuilder.addCommandHook(new Command_Main());
        commandBuilder.addCommandHook(new Command_Chunk());
        commandBuilder.addCommandHook(new Command_Set());
        commandBuilder.build();

        clog("Successfully loaded and built all commands.");


        /* Registering events */
        clog("Registering events...");

        getServer().getPluginManager().registerEvents(new InteractEvent(), this);
        getServer().getPluginManager().registerEvents(new BlockChangeByPlayerEvent(), this);
        getServer().getPluginManager().registerEvents(new BlockChangeByPistonEvent(), this);
        getServer().getPluginManager().registerEvents(new OtherBlockEvents(), this);
        getServer().getPluginManager().registerEvents(new GrowthEvents(), this);
        getServer().getPluginManager().registerEvents(new EntityEvents(), this);
        getServer().getPluginManager().registerEvents(new SignInteractEvent(), this);
        getServer().getPluginManager().registerEvents(new ConnectionEvents(), this);
        getServer().getPluginManager().registerEvents(new GuiListener(), this);
        getServer().getPluginManager().registerEvents(new CornerstoneUnloadEvent(), this);

        clog("Successfully registered events.");


        //starting auto-save
        startSave();

        //Done with all!
        clog("Successfully loaded all data and enabled " + pluginName + " v" + pluginVersion + "!");

        //DEBEG!? I mean DEBUG!? Spelling is hard lol
        Emirilda.setDebug(Settings.DEBUG.getBoolean());

    }

    @Override
    public void onDisable() {
        clog("Saving data...");

        //Saving datablock data
        dataBlockManager.saveAll();

        //Close all open inventories
        InventoryGui.getInventoriesByUUID().values().forEach(InventoryGui::closeAll);

        //Saving options data
        SavesManager.Options.saveAll();

        //Unclaiming all chunks
        getCornerstones().forEach((chunkId, cornerstone) -> {
            if(cornerstone.isClaimed()){
                assert cornerstone.getClaimedData() != null;
                Player player = getServer().getPlayer(cornerstone.getClaimedData().getOwner());
                assert player != null;
                unclaimChunk(player, getUsers().get(player.getUniqueId()));
            }
        });

        clog("Successfully saved data.");
    }

    public void save() {
        if (Settings.ANNOUNCE_AUTO_SAVE.getBoolean()) clog("Saving data...");

        //Saving datablock data
        dataBlockManager.saveAll();

        //Saving options data
        SavesManager.Options.saveAll();

        //Saving cornerstones
        getCornerstones().forEach((chunkId, cornerstone) -> {
            if (cornerstone.isClaimed()) {
                assert cornerstone.getClaimedData() != null;

                Chunk chunk = DatabaseSerializationUtility.deserializeChunkLocation(chunkId).getChunk();
                Player player = getServer().getPlayer(cornerstone.getClaimedData().getOwner());
                User user = getUsers().get(cornerstone.getClaimedData().getOwner());

                assert player != null;
                SavesManager.saveUser(player.getUniqueId(), player.getName(), user.getActingCornerstone());
                SavesManager.saveClaimedData(cornerstone.getClaimedData());
                SavesManager.savePlayerStructure(chunk, Main.getCornerstones().get(chunkId).getBaseLevel(), player.getUniqueId(), user.getActingCornerstone());

            }
        });

        if (Settings.ANNOUNCE_AUTO_SAVE.getBoolean()) clog("Successfully saved data.");
    }

    private void startSave() {
        if (Settings.AUTO_SAVE.getInt() > 0) {
            getServer().getScheduler().scheduleSyncRepeatingTask(this, this::save,
                    20, (long) Settings.AUTO_SAVE.getInt() * 60 * 20);
        }
    }

    public static void reload() {

        config.reload();
        languageManager.reload();

        InventoryGui.getInventoriesByUUID().values().forEach(InventoryGui::closeAll);
        inventories.clear();
        inventories = guiManager.loadInventories();

        Emirilda.setDebug(Settings.DEBUG.getBoolean());

    }

    private static void unclaimChunk(Player player, User user){
        String chunkId = user.getCornerstoneChunkId();
        Cornerstone cornerstone = Main.getCornerstones().get(chunkId);
        assert cornerstone.getClaimedData() != null;
        cornerstone.getClaimedData().setRestricted(true);
        Chunk chunk = DatabaseSerializationUtility.deserializeChunkLocation(chunkId).getChunk();

        SavesManager.saveUser(player.getUniqueId(), player.getName(), user.getActingCornerstone());
        SavesManager.saveClaimedData(cornerstone.getClaimedData());

        SavesManager.savePlayerStructure(chunk, Main.getCornerstones().get(chunkId).getBaseLevel(), player.getUniqueId(), user.getActingCornerstone());
        Arrays.stream(chunk.getEntities()).forEach(entity -> {
            if(!(entity instanceof Player)) entity.remove();
        });

        int x = chunk.getX()*16;
        int z = chunk.getZ()*16;
        int level = cornerstone.getBaseLevel();
        int y = level + Settings.CORNERSTONES_HEIGHT.getInt();

        while(y >= level) {
            for (int xi = 0; xi < 16; xi++)
                for (int zi = 0; zi < 16; zi++)
                    chunk.getWorld().getBlockAt(x + xi, y, z + zi).setType(Material.AIR);
            y--;
        }

        SavesManager.placeUnclaimedStructure(chunk, Main.getCornerstones().get(chunkId).getBaseLevel());

        Block block = chunk.getWorld().getBlockAt(x+8, level+1, z-1);
        block.setType(SavesManager.Options.getSign());

        Sign signBlock = (Sign) block.getState();
        org.bukkit.block.data.type.Sign signData = (org.bukkit.block.data.type.Sign) signBlock.getBlockData();
        signData.setRotation(BlockFace.NORTH);
        signBlock.setBlockData(signData);
        signBlock.setEditable(false);
        signBlock.setLine(0, Messages.SIGN_LINES_ONE.get(null));
        signBlock.setLine(1, Messages.SIGN_LINES_TWO.get(null));
        signBlock.setLine(2, Messages.SIGN_LINES_THREE.get(null));
        signBlock.setLine(3, Messages.SIGN_LINES_FOUR.get(null));
        signBlock.update();

        user.setCornerstoneChunkId("");
        cornerstone.setClaimed(false);
        cornerstone.setClaimedData(null);
    }

    private void clog(String text) {
        getLogger().info("\u001b[37m" + text);
    }

}