package com.emirilda.spigotmc.cornerstones.managers;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.ClaimedCornerstoneData;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.Gui;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.YamlInventory;
import com.emirilda.spigotmc.emirilda.utility.interfaces.Config;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

public class GuiManager {

    public static void openInventory(Player player, YamlInventory yamlInventory, ClaimedCornerstoneData claimData){
        Gui gui = new Gui(yamlInventory, claimData, player);
        Main.getPlayerGuis().put(player.getUniqueId(), gui);
        gui.open(player);
    }

    public HashMap<String, YamlInventory> loadInventories(){
        HashMap<String, YamlInventory> inventories = new HashMap<>();

        Main.getConfigManager().getConfig("inventories/main.yml").copyFromResource("inventories");
        Main.getConfigManager().getConfig("inventories/profile.yml").copyFromResource("inventories");

        File folder = new File(Main.getInstance().getDataFolder(),"inventories");

        Arrays.stream(Objects.requireNonNull(folder.listFiles())).forEach(file -> {
            if(file.getName().endsWith(".yml")){
                Config yaml = Main.getConfigManager().getConfig("inventories/"+file.getName());
                String name = file.getName().replace(".yml", "");
                inventories.put(name, YamlInventory.loadFromYaml(yaml, "inventory", name));
            }
        });

        return inventories;
    }

}
