package com.emirilda.spigotmc.cornerstones.events.griefprotection;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.utility.CornerstoneUtility;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import lombok.extern.java.Log;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;

@Log
public class BlockChangeByPistonEvent implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void pistonExtend(BlockPistonExtendEvent event){
        if(event.getBlocks().isEmpty()) return;
        String chunkId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlock().getChunk());
        String moveToId = DatabaseSerializationUtility.serializeChunkLocation(
                event.getBlocks().get(event.getBlocks().size()-1).getLocation().add(event.getDirection().getDirection()).getChunk());

        if(Main.getCornerstones().containsKey(chunkId) || Main.getCornerstones().containsKey(moveToId)){

            if(!chunkId.equals(moveToId)){
                event.setCancelled(true);
                return;
            }

            moveToId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlocks().get(event.getBlocks().size()-1).getChunk());


            if(!chunkId.equals(moveToId)){
                event.setCancelled(true);
                return;
            }


            int y = (int) (event.getBlocks().get(event.getBlocks().size()-1).getY()+Math.floor(event.getDirection().getDirection().getY()));
            if(y > CornerstoneUtility.getBuildLimit() || y < Main.getCornerstones().get(chunkId).getBaseLevel()) event.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void pistonRetract(BlockPistonRetractEvent event){
        if(event.getBlocks().isEmpty()) return;
        String chunkId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlock().getChunk());
        String moveToId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlocks().get(0).getChunk());
        if(Main.getCornerstones().containsKey(chunkId) || Main.getCornerstones().containsKey(moveToId)){

            if(!chunkId.equals(moveToId)){
                event.setCancelled(true);
                return;
            }

            int y = (event.getBlocks().get(0).getY());
            if(y > CornerstoneUtility.getBuildLimit() || y < Main.getCornerstones().get(chunkId).getBaseLevel()) {
                event.setCancelled(true);
                return;
            }

            if(event.getBlocks().size() > 1){

                moveToId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlocks().get(event.getBlocks().size()-1).getChunk());

                if(!chunkId.equals(moveToId)){
                    event.setCancelled(true);
                    return;
                }

                y = (event.getBlocks().get(event.getBlocks().size()-1).getY());
                if(y > CornerstoneUtility.getBuildLimit() || y < Main.getCornerstones().get(chunkId).getBaseLevel()) {
                    event.setCancelled(true);
                }

            }
        }
    }
}
