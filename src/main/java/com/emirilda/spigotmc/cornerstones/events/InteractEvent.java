package com.emirilda.spigotmc.cornerstones.events;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.managers.SavesManager;
import com.emirilda.spigotmc.cornerstones.utility.CornerstoneUtility;
import com.emirilda.spigotmc.cornerstones.utility.Messages;
import com.emirilda.spigotmc.cornerstones.utility.Settings;
import com.emirilda.spigotmc.cornerstones.utility.events.ChunkRegistrationFinishedEvent;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.Cornerstone;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import lombok.extern.java.Log;
import org.bukkit.Chunk;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Objects;

@Log
public class InteractEvent implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void interact(PlayerInteractEvent event) {
        try {
            Player player = event.getPlayer();
            if (Main.getUserQueue().containsKey(player.getUniqueId())) {
                if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
                    Block block = event.getClickedBlock();
                    assert block != null;
                    Chunk chunk = block.getChunk();
                    event.setCancelled(true);
                    switch (Main.getUserQueue().get(player.getUniqueId()).queueReason()) {

                        case REGISTER_CHUNK -> {
                            int buildLimit = Settings.BUILD_LIMIT.getInt()-Settings.CORNERSTONES_HEIGHT.getInt();
                            //If player tries to register a cornerstone chunk above the allowed build limit
                            if(block.getY() >= buildLimit){
                                player.sendMessage(Messages.REGISTER_TOO_HIGH.get(player, buildLimit));
                                break;
                            }
                            if(CornerstoneUtility.isBlockedByChunk(chunk)){
                                player.sendMessage(Messages.REGISTER_BLOCKED.get(player));
                                break;
                            }
                            //The method registerChunk will return true if it was able to register the chunk, if the chunk is already registered it will return false
                            if(SavesManager.registerChunk(chunk, block.getY())){
                                player.sendMessage(Messages.REGISTER_REGISTERING.get(player, chunk.getX() + ", " + chunk.getZ(), block.getY()));
                                CornerstoneUtility.registerChunk(chunk, block.getY(), player);
                            }else
                                player.sendMessage(Messages.REGISTER_ALREADY.get(player));
                        }

                        case UNREGISTER_CHUNK -> {
                            //Makes sure that the Cornerstone is not claimed before unregistering it
                            if(Main.getCornerstones().containsKey(DatabaseSerializationUtility.serializeChunkLocation(chunk))){
                                Cornerstone cornerstone = Main.getCornerstones().get(DatabaseSerializationUtility.serializeChunkLocation(chunk));
                                if(cornerstone.isClaimed()){
                                    assert cornerstone.getClaimedData() != null;
                                    CornerstoneUtility.unclaimChunk(
                                            Objects.requireNonNull(Main.getInstance().getServer().getPlayer(cornerstone.getClaimedData().getOwner())),
                                            Main.getUsers().get(cornerstone.getClaimedData().getOwner()));
                                }
                            }
                            //The method unregisterChunk will return true if it was able to unregister the chunk, if the chunk is not registered it will return false
                            Main.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
                                if(SavesManager.unregisterChunk(chunk)) {
                                    CornerstoneUtility.unregisterChunk(chunk, block.getY());
                                    player.sendMessage(Messages.REGISTER_UNREGISTERED.get(player, chunk.getX() + ", " + chunk.getZ()));
                                }else
                                    player.sendMessage(Messages.REGISTER_NOT.get(player));
                            }, 20);
                        }

                        case SET_STARTER -> {

                            if(!SavesManager.isRegisteredChunk(chunk)){
                                player.sendMessage(Messages.REGISTER_NOT.get(player));
                                break;
                            }
                            SavesManager.saveStarterStructure(chunk, SavesManager.getChunkLevel(chunk));
                            player.sendMessage(Messages.SET_STARTER.get(player));

                        }

                        case SET_UNCLAIMED -> {

                            if(!SavesManager.isRegisteredChunk(chunk)){
                                player.sendMessage(Messages.REGISTER_NOT.get(player));
                                break;
                            }
                            SavesManager.saveUnclaimedStructure(chunk, SavesManager.getChunkLevel(chunk));
                            player.sendMessage(Messages.SET_UNCLAIMED.get(player));

                        }

                    }
                } else {
                    player.sendMessage(Messages.REGISTER_CANCELLED.get(player));
                }
                Main.getUserQueue().remove(player.getUniqueId());
            }
        }catch (Exception exception){
            Main.getUserQueue().remove(event.getPlayer().getUniqueId());
            MessageUtility.logException(exception, getClass(), log);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void finished(ChunkRegistrationFinishedEvent event){
        event.getCaller().sendMessage(Messages.REGISTER_FINISHED.get(event.getCaller(), event.getChunk().getX()+", " + event.getChunk().getZ()));
    }

}
