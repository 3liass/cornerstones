package com.emirilda.spigotmc.cornerstones.events;

import com.emirilda.spigotmc.cornerstones.utility.interfaces.InventoryGui;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.YamlItem;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import lombok.extern.java.Log;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Objects;
import java.util.UUID;

@Log
public class GuiListener implements Listener {

    /* Basic click and drag events, executes action if found */

    private void inventoryInteractEvent (InventoryInteractEvent rawEvent) {
        try {
            InventoryClickEvent event = (InventoryClickEvent) rawEvent;

            if (!(event.getWhoClicked() instanceof Player player)) return;

            UUID playerUUID = player.getUniqueId();
            UUID inventoryUUID = InventoryGui.getOpenInventories().get(playerUUID);

            if (inventoryUUID != null) {
                InventoryGui gui = InventoryGui.getInventoriesByUUID().get(inventoryUUID);
                if (gui.getType() == InventoryGui.GUIType.UNEDITABLE) { event.setCancelled(true); }
                InventoryGui.GUIAction action = gui.getActions().get(event.getSlot());
                if (action != null && Objects.requireNonNull(event.getClickedInventory()).getType() != InventoryType.PLAYER) {
                    YamlItem item = gui.getYamlInventory().getItems().get(event.getSlot());
                    if(item != null && player.hasPermission(item.getPermission()) && item.isUse()){
                        String permission = gui.getYamlInventory().getItems().get(event.getSlot()).getAction().permission();
                        action.perform(player, event, gui.getClaimData(), permission);
                    }else
                        action.click(player, event, gui.getClaimData());
                }
            }

        } catch (Exception exception) { MessageUtility.logException(exception, getClass(), log); }
    }

    @EventHandler
    public void onClick (InventoryClickEvent event) { inventoryInteractEvent(event); }

    /* Events for removing the player from the openInventories list if they exit the inventory */
    @EventHandler
    public void onClose (InventoryCloseEvent event) {
        try {
            Player player = (Player) event.getPlayer();
            UUID uuid = player.getUniqueId();
            if (InventoryGui.getOpenInventories().containsKey(uuid)) {
                InventoryGui.getInventoriesByUUID().get(InventoryGui.getOpenInventories().get(uuid)).closeExecutions(player);
                InventoryGui.getOpenInventories().remove(uuid);
            }
        } catch (Exception exception) { MessageUtility.logException(exception, getClass(), log); }
    }

    @EventHandler
    public void onQuit (PlayerQuitEvent event) {
        try {
            Player player = event.getPlayer();
            UUID uuid = player.getUniqueId();
            if (InventoryGui.getOpenInventories().containsKey(uuid)) {
                InventoryGui.getInventoriesByUUID().get(InventoryGui.getOpenInventories().get(uuid)).closeExecutions(player);
                InventoryGui.getOpenInventories().remove(uuid);
            }
        } catch (Exception exception) { MessageUtility.logException(exception, getClass(), log); }
    }

}
