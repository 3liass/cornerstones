package com.emirilda.spigotmc.cornerstones.utility.interfaces;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.managers.GuiManager;
import com.emirilda.spigotmc.cornerstones.utility.Messages;
import com.emirilda.spigotmc.emirilda.utility.ItemUtility;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import com.emirilda.spigotmc.emirilda.utility.interfaces.Config;
import com.emirilda.spigotmc.emirilda.wesjd.anvilgui.AnvilGUI;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.java.Log;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import java.util.*;

@Data @AllArgsConstructor @Log
public class YamlItem {
    private String id;

    private boolean use;

    private String material;

    private String name;

    private List<String> lore;

    private Action action;
    private Action normalAction;

    private int slot;

    private ItemType type;
    private String value;

    private String permission;

    private boolean useCache;

    public ItemStack createItemStack(Player player, ClaimedCornerstoneData claimData, List<YamlItem> items, HashMap<String, ItemStack> cache){
        if(cache.containsKey(id)) return cache.get(id);
        ItemStack itemStack = getItemStack(player, claimData, items, cache);
        if(useCache) cache.put(id, itemStack);
        return itemStack;
    }
    private ItemStack getItemStack(Player player, ClaimedCornerstoneData claimData, List<YamlItem> items, HashMap<String, ItemStack> cache) {

        String fixedName;

        List<String> fixedLore = new ArrayList<>();

        fixedName = MessageUtility.colorizeMessage(player, name);
        for(String loreString : lore){
            fixedLore.add(MessageUtility.colorizeMessage(player, loreString));
        }

        switch (type){
            case NORMAL -> {
                return ItemUtility.createItem(Objects.requireNonNull(Material.getMaterial(material.toUpperCase())), fixedName, fixedLore);
            }
            case HEAD -> {

                ItemStack itemStack = new ItemStack(Material.PLAYER_HEAD);
                SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();
                assert skullMeta != null;
                String headName = "";

                if(value.equalsIgnoreCase("user")){
                    skullMeta.setOwningPlayer(player);
                    headName = player.getName();
                }else if(value.startsWith("trusted:")){
                    String[] values = value.split("[|]");
                    String backupItemId = values[1];
                    String[] trustedValues = values[0].split(":");
                    int trustedIndex = Integer.parseInt(trustedValues[1]);

                    if(claimData.getTrustedUsers().size() < trustedIndex){
                        Optional<YamlItem> backupItemOptional = items.stream().filter(item -> item.id.equals(backupItemId)).findFirst();
                        if (backupItemOptional.isEmpty()) return null;
                        YamlItem backupItem = backupItemOptional.get();
                        action = backupItem.action;
                        return backupItem.createItemStack(player, claimData, items, cache);
                    }else{
                        action = normalAction;
                        OfflinePlayer trustedPlayer = Main.getInstance().getServer().getOfflinePlayer(claimData.getTrustedUsers().get(trustedIndex-1));
                        skullMeta.setOwningPlayer(trustedPlayer);
                        headName = trustedPlayer.getName();
                    }
                }

                itemStack.setItemMeta(skullMeta);
                List<String> namedLore = new ArrayList<>();
                String finalHeadName = headName;
                fixedLore.forEach(lorePiece -> {
                    assert finalHeadName != null;
                    namedLore.add(lorePiece.replace("{head}", finalHeadName));
                });

                assert headName != null;
                return ItemUtility.createItem(itemStack, fixedName.replace("{head}", headName), namedLore);
            }
            default -> {
                return null;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static YamlItem loadFromYaml(Config yaml, String path, String id) {
        path+=".";

        boolean use = false;
        boolean useCache = true;
        ItemType itemType = ItemType.NORMAL;
        String value = "";
        String material = "stone";
        String name = "Item";
        int slot = 0;
        List<String> lore = new ArrayList<>();
        Action action = null;
        String permissionName = "cornerstones.use";

        if (yaml.get().contains(path + "cache"))
            useCache = (boolean) yaml.get(path + "cache");

        if (yaml.get().contains(path + "value"))
            value = (String) yaml.get(path + "value");

        if (yaml.get().contains(path + "type"))
            itemType = ItemType.valueOf(((String) yaml.get(path + "type")).toUpperCase());

        if (yaml.get().contains(path + "action"))
            action = Action.loadFromYaml(yaml, path + "action");


        if (yaml.get().contains(path + "lore"))
            lore = (List<String>) yaml.get(path + "lore");

        if (yaml.get().contains(path + "item_name"))
            name = (String) yaml.get(path + "item_name");

        if (yaml.get().contains(path + "item"))
            material = (String) yaml.get(path + "item");

        if (yaml.get().contains(path + "use"))
            use = (boolean) yaml.get(path + "use");

        if (yaml.get().contains(path + "permission"))
            permissionName = (String) yaml.get(path + "permission");

        if (yaml.get().contains(path + "slot")){
            String rawSlot = (String) yaml.get(path + "slot");

            if(rawSlot.contains("|")){
                String[] rawSlotSplit = rawSlot.split("[|]");
                int row = Integer.parseInt(rawSlotSplit[0]);
                int rowPosition = Integer.parseInt(rawSlotSplit[1]);

                slot = (row-1)*9+rowPosition-1;
            }else{
                slot = Integer.parseInt(rawSlot);
            }
        }

        Permission permission = new Permission(permissionName, PermissionDefault.FALSE);

        if (Main.getInstance().getServer().getPluginManager().getPermission(permissionName) == null) {
            Main.getInstance().getServer().getPluginManager().addPermission(permission);
        }

        return new YamlItem(id, use, material, name, lore, action, action, slot, itemType, value, permissionName, useCache);
    }

    public enum ItemType {
        NORMAL, HEAD
    }

    public record Action(InventoryGui.GUIAction action, String permission) {

        public static Action loadFromYaml(Config yaml, String path) {
            path += ".";

            InventoryGui.GUIAction action;

            ActionType actionType = ActionType.valueOf(((String) yaml.get(path + "type")).toUpperCase());
            boolean cancel = (boolean) yaml.get(path + "cancel_event");
            String value = (String) yaml.get(path + "value");
            String permission = "cornerstones.use";

            if (yaml.get().contains(path + "permission"))
                permission = (String) yaml.get(path + "permission");

            switch (actionType) {
                case RUN_COMMAND -> action = (player, clickEvent, claimData) -> {
                    if (cancel) clickEvent.setCancelled(true);
                    player.performCommand(value);
                };
                case OPEN_INVENTORY -> action = (player, clickEvent, claimData) -> {
                    if (cancel) clickEvent.setCancelled(true);
                    GuiManager.openInventory(player, Main.getInventories().get(value), claimData);
                };
                case CLOSE_INVENTORY -> action = (player, clickEvent, claimData) -> {
                    if (cancel) clickEvent.setCancelled(true);
                    player.closeInventory();
                };
                case SERVER_COMMAND -> action = (player, clickEvent, claimData) -> {
                    if (cancel) clickEvent.setCancelled(true);
                    Bukkit.dispatchCommand(Main.getInstance().getServer().getConsoleSender(), value);
                };
                case SEND_MESSAGE -> action = (player, clickEvent, claimData) -> {
                    if (cancel) clickEvent.setCancelled(true);
                    player.sendMessage(MessageUtility.colorizeMessage(player, value));
                };
                case NONE, CANCEL, CONFIRM -> action = (player, clickEvent, claimData) -> {
                    if (cancel) clickEvent.setCancelled(true);
                };
                case TRUST_USER -> action = (clickPlayer, clickEvent, claimData) -> {
                    if (cancel) clickEvent.setCancelled(true);

                    AnvilGUI.Builder builder = new AnvilGUI.Builder();
                    builder.onComplete((player, text) -> {
                                String name = text.replaceAll(" ", "");
                                Optional<OfflinePlayer> target = Arrays.stream(Main.getInstance().getServer().getOfflinePlayers())
                                        .filter(offlinePlayer -> Objects.requireNonNull(offlinePlayer.getName()).equalsIgnoreCase(name)).findFirst();

                                ItemStack headItem = new ItemStack(Material.PLAYER_HEAD);
                                String headLore;
                                String headName;
                                if (target.isPresent()) {
                                    //Setting the skull to the correct players head
                                    SkullMeta skullMeta = (SkullMeta) headItem.getItemMeta();
                                    assert skullMeta != null;
                                    skullMeta.setOwningPlayer(target.get());
                                    headItem.setItemMeta(skullMeta);
                                    headName = Messages.USAGE_TRUST_NAME.get(player, target.get().getName());
                                    //If it should actually trust anyone or not
                                    boolean trust = false;

                                    //If trying to trust themselves
                                    if (target.get().getUniqueId() == player.getUniqueId()) {
                                        headLore = Messages.USAGE_TRUST_SELF.get(player);

                                        //If trying to trust a player who is already trusted
                                    } else if (claimData.getTrustedUsers().contains(target.get().getUniqueId())) {
                                        headLore = Messages.USAGE_TRUST_ALREADY.get(player, target.get().getName());

                                        //If the trust shall succeed
                                    } else {
                                        headLore = Messages.USAGE_TRUST_GRANT.get(player, target.get().getName());
                                        trust = true;
                                    }

                                    //Setting the action of when head is clicked
                                    boolean finalTrust = trust;
                                    builder.onRightInputClick(finishPlayer -> {
                                        //If trust is true, meaning a player should get trusted, then the target player will get trusted
                                        if (finalTrust) {
                                            claimData.getTrustedUsers().add(target.get().getUniqueId());
                                            finishPlayer.sendMessage(Messages.USAGE_TRUST_PLAYER.get(finishPlayer, target.get().getName()));
                                            if (target.get().isOnline()) {
                                                Objects.requireNonNull(target.get().getPlayer()).
                                                        sendMessage(Messages.USAGE_TRUST_GAINED.get(target.get().getPlayer(), finishPlayer.getName()));
                                            }
                                        }

                                        //No matter the outcome of whom to trust or not to trust (that's the question) the anvil gui will get closed
                                        GuiManager.openInventory(finishPlayer, Main.getInventories().get("main"), claimData);
                                    });

                                    //If they player they are trying to trust doesn't actually exist
                                } else {
                                    headName = Messages.USAGE_TRUST_INVALID_NAME.get(player, name);
                                    headLore = Messages.USAGE_TRUST_INVALID.get(player, name);
                                    builder.onRightInputClick(finishPlayer -> GuiManager.openInventory(finishPlayer, Main.getInventories().get("main"), claimData));
                                }

                                //This basically sets the head in the middle based on the previous checks
                                builder.itemRight(ItemUtility.createItem(headItem, headName, List.of(headLore.split("\n"))));
                                builder.open(player);
                                return AnvilGUI.Response.text(" ");
                            })
                            .itemLeft(ItemUtility.createItem(Material.PAPER, " "))
                            .title(Messages.USAGE_TRUST_TITLE.get(clickPlayer))
                            .plugin(Main.getInstance());
                    builder.open(clickPlayer);

                };
                case REVOKE_TRUST -> action = (player, clickEvent, claimData) -> {
                    if (cancel) clickEvent.setCancelled(true);

                    if (clickEvent.isShiftClick())
                        if (value.startsWith("trusted:")) {
                            String[] values = value.split(":");
                            int trustedIndex = Integer.parseInt(values[1]);

                            if (claimData.getTrustedUsers().size() >= trustedIndex) {
                                OfflinePlayer target = Main.getInstance().getServer().getOfflinePlayer(claimData.getTrustedUsers().get(trustedIndex - 1));
                                claimData.getTrustedUsers().remove(target.getUniqueId());
                                player.sendMessage(Messages.USAGE_TRUST_PLAYER_REVOKED.get(player, target.getName()));
                                if (target.isOnline()) {
                                    Objects.requireNonNull(target.getPlayer()).sendMessage(Messages.USAGE_TRUST_REVOKED.get(target.getPlayer(), player.getName()));
                                }
                                Main.getPlayerGuis().get(player.getUniqueId()).getCache().clear();
                                Main.getPlayerGuis().get(player.getUniqueId()).updateItems();
                            }
                        }
                };
                default -> throw new IllegalStateException("Unexpected value: " + actionType);
            }

            Permission perm = new Permission(permission, PermissionDefault.FALSE);

            if (Main.getInstance().getServer().getPluginManager().getPermission(permission) == null) {
                Main.getInstance().getServer().getPluginManager().addPermission(perm);
            }

            return new Action(action, permission);

        }

        public enum ActionType {
            OPEN_INVENTORY, CLOSE_INVENTORY, RUN_COMMAND, NONE, SERVER_COMMAND, SEND_MESSAGE, TRUST_USER, REVOKE_TRUST, CANCEL, CONFIRM
        }
    }
}
