package com.emirilda.spigotmc.cornerstones.utility.events;

import lombok.Getter;
import lombok.NonNull;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ChunkRegistrationFinishedEvent extends Event {

    private static final HandlerList HANDLERS_LIST = new HandlerList();

    @Getter
    private final Player caller;
    @Getter
    private final Chunk chunk;


    public ChunkRegistrationFinishedEvent(Player caller, Chunk chunk){
        this.caller = caller;
        this.chunk = chunk;
    }


    @Override
    public @NonNull HandlerList getHandlers() {
        return HANDLERS_LIST;
    }

    @SuppressWarnings("unused")
    public static HandlerList getHandlerList() {
        return HANDLERS_LIST;
    }

}
