package com.emirilda.spigotmc.cornerstones.utility.interfaces;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.annotation.Nullable;

@Data @AllArgsConstructor
public class Cornerstone {

    private int baseLevel;
    private boolean claimed;
    @Nullable private ClaimedCornerstoneData claimedData;

}
