package com.emirilda.spigotmc.cornerstones.utility;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import org.bukkit.entity.Player;

public enum Messages {

    /* General Plugin Messages */
    PREFIX                              ("plugin.prefix"),
    RELOADED                            ("plugin.reloaded"),

    //General "no..."
    NO_CONSOLE                          ("general_no.console"),
    NO_PERMISSION                       ("general_no.permission"),



    /* Chunk messages */
    REGISTER_CLICK_TO_REGISTER          ("chunk.register.click_to_register"),
    REGISTER_CLICK_TO_UNREGISTER        ("chunk.register.click_to_unregister"),
    REGISTER_CANCELLED                  ("chunk.register.cancelled"),
    REGISTER_ALREADY                    ("chunk.register.already"),
    REGISTER_NOT                        ("chunk.register.not"),
    REGISTER_UNREGISTERED               ("chunk.register.unregistered"),
    REGISTER_TOO_HIGH                   ("chunk.register.too_high"),
    REGISTER_FINISHED                   ("chunk.register.finished"),
    REGISTER_REGISTERING                ("chunk.register.registering"),
    REGISTER_BLOCKED                    ("chunk.register.blocked"),
    REGISTER_SIMULTANEOUS_REGISTER      ("chunk.register.simultaneous_register"),
    REGISTER_SIMULTANEOUS_UNREGISTER    ("chunk.register.simultaneous_unregister"),

    ALREADY_IN_QUEUE                    ("chunk.register.already_in_queue"),


    /* Set */
    SET_NO_MATERIAL                     ("chunk.set.no_material"),
    SET_BASE                            ("chunk.set.base"),
    SET_SIGN                            ("chunk.set.sign"),
    SET_OPTIONS                         ("chunk.set.options"),
    SET_MUST_BE_SIGN                    ("chunk.set.must_be_sign"),
    SET_MUST_BE_BLOCK("chunk.set.must_be_block"),
    SET_SIMULTANEOUS("chunk.set.simultaneous"),
    SET_STARTER_CLICK("chunk.set.click_to_starter"),
    SET_UNCLAIMED_CLICK("chunk.set.click_to_unclaimed"),
    SET_STARTER("chunk.set.starter"),
    SET_UNCLAIMED("chunk.set.unclaimed"),


    /* Sign lines */
    SIGN_LINES_ONE("chunk.sign_lines.one"),
    SIGN_LINES_TWO("chunk.sign_lines.two"),
    SIGN_LINES_THREE("chunk.sign_lines.three"),
    SIGN_LINES_FOUR("chunk.sign_lines.four"),


    /* Usage */
    USAGE_CLAIMED("chunk.usage.claimed"),
    USAGE_CLAIMED_BY("chunk.usage.claimed_by"),
    USAGE_CLAIMED_BY_AND_TRUSTED("chunk.usage.claimed_by_and_trusted"),
    USAGE_AND("chunk.usage.and"),
    USAGE_NOT_CLAIMED("chunk.usage.not_claimed"),

    USAGE_BUILD_LIMIT("chunk.usage.build_limit"),
    USAGE_COOLDOWN("chunk.usage.cooldown"),

    USAGE_TRUST_TITLE("chunk.usage.trust.title"),
    USAGE_TRUST_NAME("chunk.usage.trust.name"),
    USAGE_TRUST_GRANT("chunk.usage.trust.grant"),
    USAGE_TRUST_PLAYER("chunk.usage.trust.player"),
    USAGE_TRUST_PLAYER_REVOKED("chunk.usage.trust.player_revoked"),
    USAGE_TRUST_GAINED("chunk.usage.trust.gained"),
    USAGE_TRUST_REVOKED("chunk.usage.trust.revoked"),
    USAGE_TRUST_ALREADY("chunk.usage.trust.already"),
    USAGE_TRUST_SELF("chunk.usage.trust.self"),
    USAGE_TRUST_INVALID("chunk.usage.trust.invalid"),
    USAGE_TRUST_INVALID_NAME("chunk.usage.trust.invalid_name");

    private final String path;

    Messages(String path) {
        this.path = path;
    }

    private String getRaw() {
        return Main.getLanguageManager().getString(path);
    }

    private String getReplaced(){ return Main.getLanguageManager().getString(path).replace("{prefix}", PREFIX.getRaw()).replace("{nl}", "\n").replace("\\n", "\n"); }
    public String get(Player player){
        return MessageUtility.colorizeMessage(player, getReplaced());
    }
    public String get(Player player, Object... args){
        return String.format(MessageUtility.colorizeMessage(player, getReplaced()), args);
    }

}