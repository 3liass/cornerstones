package com.emirilda.spigotmc.cornerstones.utility.interfaces;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data @AllArgsConstructor
public class ClaimedCornerstoneData {

    private UUID owner;
    private List<UUID> trustedUsers;
    private boolean restricted;
    private int id;

}
