package com.emirilda.spigotmc.cornerstones.commands;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.managers.SavesManager;
import com.emirilda.spigotmc.cornerstones.utility.CornerstoneUtility;
import com.emirilda.spigotmc.cornerstones.utility.Messages;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.QueueUser;
import com.emirilda.spigotmc.emirilda.commands.builder.CommandHookExecutor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class Command_Set {

    /* Options values */
    @CommandHookExecutor("cornerstones_chunk_set_base")
    public void base(CommandSender sender, String input){

        if(sender instanceof Player player) {
            Material material = Material.matchMaterial(input);

            if (material == null) {
                sender.sendMessage(Messages.SET_NO_MATERIAL.get(player, input));
                return;
            }

            if (!material.isBlock()) {
                sender.sendMessage(Messages.SET_MUST_BE_BLOCK.get(player));
                return;
            }

            SavesManager.Options.setBase(material);
            sender.sendMessage(Messages.SET_BASE.get(player, material.name()));
        } else {
            sender.sendMessage(Messages.NO_CONSOLE.get(null));
        }
    }

    @CommandHookExecutor("cornerstones_chunk_set_sign")
    public void sign(CommandSender sender, String input){

        if(sender instanceof Player player) {

            Material material = Material.matchMaterial(input);

            if (material == null) {
                sender.sendMessage(Messages.SET_NO_MATERIAL.get(player, input));
                return;
            }

            if (!(material.name().contains("SIGN"))) {
                sender.sendMessage(Messages.SET_MUST_BE_SIGN.get(player));
                return;
            }

            SavesManager.Options.setSign(material);
            sender.sendMessage(Messages.SET_SIGN.get(player, material.name()));
        }else{
            sender.sendMessage(Messages.NO_CONSOLE.get(null));
        }
    }

    @CommandHookExecutor("cornerstones_chunk_set_options")
    public void options(CommandSender sender, String input){

        if(sender instanceof Player player) {

            Material material = Material.matchMaterial(input);

            if (material == null) {
                sender.sendMessage(Messages.SET_NO_MATERIAL.get(player, input));
                return;
            }

            if (!material.isBlock()) {
                sender.sendMessage(Messages.SET_MUST_BE_BLOCK.get(player));
                return;
            }

            SavesManager.Options.setOption_block(material);
            sender.sendMessage(Messages.SET_OPTIONS.get(player, material.name()));
        }else{
            sender.sendMessage(Messages.NO_CONSOLE.get(null));
        }
    }



    /* Structure values */

    @CommandHookExecutor("cornerstones_chunk_set_starter")
    public void starter(CommandSender sender){
        if(sender instanceof Player player){
            if(CornerstoneUtility.isCurrentlyRegistering()){
                player.sendMessage(Messages.SET_SIMULTANEOUS.get(player));
                return;
            }
            if(Main.getUserQueue().containsKey(player.getUniqueId())){
                player.sendMessage(Messages.ALREADY_IN_QUEUE.get(player));
                return;
            }
            player.sendMessage(Messages.SET_STARTER_CLICK.get(player));
            Main.getUserQueue().put(player.getUniqueId(), new QueueUser(QueueUser.QueueReasons.SET_STARTER));
        }else{
            sender.sendMessage(Messages.NO_CONSOLE.get(null));
        }
    }

    @CommandHookExecutor("cornerstones_chunk_set_unclaimed")
    public void unclaimed(CommandSender sender){
        if(sender instanceof Player player){
            if(CornerstoneUtility.isCurrentlyRegistering()){
                player.sendMessage(Messages.SET_SIMULTANEOUS.get(player));
                return;
            }
            if(Main.getUserQueue().containsKey(player.getUniqueId())){
                player.sendMessage(Messages.ALREADY_IN_QUEUE.get(player));
                return;
            }
            player.sendMessage(Messages.SET_UNCLAIMED_CLICK.get(player));
            Main.getUserQueue().put(player.getUniqueId(), new QueueUser(QueueUser.QueueReasons.SET_UNCLAIMED));
        }else{
            sender.sendMessage(Messages.NO_CONSOLE.get(null));
        }
    }
}