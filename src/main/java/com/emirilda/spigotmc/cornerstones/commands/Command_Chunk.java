package com.emirilda.spigotmc.cornerstones.commands;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.utility.CornerstoneUtility;
import com.emirilda.spigotmc.cornerstones.utility.Messages;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.Cornerstone;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.QueueUser;
import com.emirilda.spigotmc.emirilda.commands.builder.CommandHookExecutor;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import com.emirilda.spigotmc.emirilda.utility.TextUtility;
import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Objects;

@SuppressWarnings("unused")
public class Command_Chunk {

    @CommandHookExecutor("cornerstones_chunk_info")
    public void info(CommandSender sender) {
        if (sender instanceof Player player) {

            Chunk chunk = player.getLocation().getChunk();

            if (!Main.getCornerstones().containsKey(DatabaseSerializationUtility.serializeChunkLocation(chunk))) {
                player.sendMessage(Messages.REGISTER_NOT.get(player));
                return;
            }

            Cornerstone cornerstone = Main.getCornerstones().get(DatabaseSerializationUtility.serializeChunkLocation(chunk));

            if (cornerstone.isClaimed()) {
                assert cornerstone.getClaimedData() != null;
                String owner = Objects.requireNonNull(Main.getInstance().getServer().getPlayer(cornerstone.getClaimedData().getOwner())).getName();
                if (cornerstone.getClaimedData().getTrustedUsers().size() > 0) {
                    String trusted = TextUtility.readableJoin(Messages.USAGE_AND.get(player),
                            cornerstone.getClaimedData().getTrustedUsers().stream().map(
                                    user -> Main.getInstance().getServer().getOfflinePlayer(user).getName()
                            ).toArray(String[]::new)
                    );
                    player.sendMessage(Messages.USAGE_CLAIMED_BY_AND_TRUSTED.get(player, owner, trusted));
                } else
                    player.sendMessage(Messages.USAGE_CLAIMED_BY.get(player, owner));
            } else
                player.sendMessage(Messages.USAGE_NOT_CLAIMED.get(player));


        } else {
            sender.sendMessage(Messages.NO_CONSOLE.get(null));
        }
    }

    @CommandHookExecutor("cornerstones_chunk_register")
    public void register(CommandSender sender){
        if(sender instanceof Player player){
            if(CornerstoneUtility.isCurrentlyRegistering()){
                player.sendMessage(Messages.REGISTER_SIMULTANEOUS_REGISTER.get(player));
                return;
            }
            if(Main.getUserQueue().containsKey(player.getUniqueId())){
                player.sendMessage(Messages.ALREADY_IN_QUEUE.get(player));
                return;
            }
            player.sendMessage(Messages.REGISTER_CLICK_TO_REGISTER.get(player));
            Main.getUserQueue().put(player.getUniqueId(), new QueueUser(QueueUser.QueueReasons.REGISTER_CHUNK));
        }else{
            sender.sendMessage(Messages.NO_CONSOLE.get(null));
        }
    }

    @CommandHookExecutor("cornerstones_chunk_unregister")
    public void unregister(CommandSender sender){
        if(sender instanceof Player player){
            if(CornerstoneUtility.isCurrentlyRegistering()){
                player.sendMessage(Messages.REGISTER_SIMULTANEOUS_UNREGISTER.get(player));
                return;
            }
            if(Main.getUserQueue().containsKey(player.getUniqueId())){
                player.sendMessage(Messages.ALREADY_IN_QUEUE.get(player));
                return;
            }
            player.sendMessage(Messages.REGISTER_CLICK_TO_UNREGISTER.get(player));
            Main.getUserQueue().put(player.getUniqueId(), new QueueUser(QueueUser.QueueReasons.UNREGISTER_CHUNK));
        }else{
            sender.sendMessage(Messages.NO_CONSOLE.get(null));
        }
    }

    @CommandHookExecutor("cornerstones_chunk_list")
    public void list(CommandSender sender){

        sender.sendMessage("chunk list");

    }


}
